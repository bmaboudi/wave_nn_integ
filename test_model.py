import numpy as np
import torch
import high_fidelity as hf
import nn_network as neural_net
import matplotlib.pyplot as plt

if __name__=='__main__':
	nn = neural_net.network_eval('./model.pt')
	nn_eval = nn.eval_model

	prob = hf.high_fidelity(100)
	prob.initialize_FD()
	c = np.random.uniform(1,4)
	sol_q, sol_p = prob.symplectic_euler(c)

	in_size = sol_q[0].shape[0]

	in_data = np.zeros(2*in_size+1)
	in_data[0] = c
	in_data[1:in_size+1] = sol_q[0]
	in_data[in_size+1:] = sol_p[0]

	in_list = []
	in_list = [np.copy(in_data[1:])]
#	print(in_list[0])
#	input()

	MAX_ITER = len(sol_q)

	for i in range(MAX_ITER-1):
		torch_in = torch.from_numpy( np.reshape(in_data,[1,-1]) ).float()

		out_vec = nn_eval(torch_in)
		in_list.append(np.copy(out_vec))
		in_data[1:] = out_vec

	in_list = np.reshape(in_list,[len(in_list),-1])
	#in_list = np.copy(in_list[:,:in_size])
	exact = np.reshape(sol_q,[MAX_ITER,-1])

	np.savez('results.npz', exact=exact , in_list=in_list[:,:in_size])
import numpy as np
import numpy.linalg as la
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import argparse
import progressbar
import data_handler as data
import os.path
from torch.optim.lr_scheduler import ReduceLROnPlateau

class network(nn.Module):
    def __init__( self, input_size, output_size ):
        super().__init__()
        self.fc1 = nn.Linear(input_size, 200)
        self.fc2 = nn.Linear(200, 200)
        self.fc3 = nn.Linear(200, 200)
        self.fc4 = nn.Linear(200, 200)
        self.fc5 = nn.Linear(200, output_size)

    def forward( self, x ):
        x = F.relu( self.fc1(x) )
        x = F.relu( self.fc2(x) )
        x = F.relu( self.fc3(x) )
        x = F.relu( self.fc4(x) )
        return self.fc5(x)

class Loss(nn.Module):
    def __init__( self, N, dx, batch_size ):
        super().__init__()
        self.sig = nn.Sigmoid()
        self.N = N
        self.dx = dx

        d1 = torch.ones(100)
        d2 = torch.ones(100-1)

        self.Dx1 = torch.diag(d1,diagonal=0) - torch.diag(d2,diagonal=-1)
        self.Dx1[0,-1] = -1
        self.Dx1 = self.Dx1/dx
        self.Dx1 = torch.reshape(self.Dx1, [1,100,100])
        self.Dx1 = self.Dx1.repeat(batch_size,1,1)

        self.Dx2 = torch.diag(d2,diagonal=1) - torch.diag(d1,diagonal=0)
        self.Dx2[-1,0] = 1
        self.Dx2 = self.Dx2/dx
        self.Dx2 = torch.reshape(self.Dx2, [1,100,100])
        self.Dx2 = self.Dx2.repeat(batch_size,1,1)


    def forward( self, y, y_ , c):
        q = (y[:,:self.N]).unsqueeze(2)
        p = y[:,self.N:]

        q_ = (y_[:,:self.N]).unsqueeze(2)
        p_ = y_[:,self.N:]

        qx1 = (torch.matmul(self.Dx1,q)).squeeze(2)
        qx2 = (torch.matmul(self.Dx2,q)).squeeze(2)

        qx1_ = (torch.matmul(self.Dx1,q_)).squeeze(2)
        qx2_ = (torch.matmul(self.Dx2,q_)).squeeze(2)

        pot = 0.5*c*( (qx1**2).sum(axis=1) + (qx2**2).sum(axis=1) )*self.dx
        kin = (p**2).sum(axis=1)*self.dx

        pot_ = 0.5*c*( (qx1_**2).sum(axis=1) + (qx2_**2).sum(axis=1) )*self.dx
        kin_ = (p_**2).sum(axis=1)*self.dx


        diff1 = (( y - y_ ) ** 2).sum(axis=1)
        diff2 = torch.abs( y - y_ ).sum(axis=1)
        diff3 = torch.abs( (kin - kin_) + (pot - pot_) )

        #print(torch.mean(diff1))
        #print(torch.mean(diff2))
        #print(torch.mean(diff3))
        #input()

        diff1 = self.sig(diff1 - 0.005)*diff1
        diff2 = self.sig(diff2 - 0.7)*diff2
        diff3 = self.sig(diff3 - 0.05)*diff3

        #loss =  self.sig( 0.9*diff1 + 0.05*diff2 + 0.05*diff3 - 0.02 ) * (0.95*diff1 + 0.05*diff2)
        loss = ( 0.89*diff1 + 0.01*diff2 + 0.1*diff3 )
        return loss.sum()/y.size()[0]/y.size()[1]

def train_model(model_path, nn_input_size, nn_output_size):
    path = 'data_raw.npz'
    batch_size = 256
    train_data = data.data_holder(batch_size=batch_size)
    train_data.read_from_file('data_raw.npz')

    #path = 'test_data.npz'
    #test_data = data.data(0, 'blob', batch_size = 64, num_batches= 100)
    #test_data.load_data(path)

    model = network( nn_input_size, nn_output_size )

    e_local = list()
    if( os.path.isfile(model_path) == True ):
        model.load_state_dict(torch.load(model_path))

    device = torch.device('cpu')
    #optimizer = optim.SGD(model.parameters(), lr = 0.001)
    optimizer = optim.Adam(model.parameters(),lr=0.00001)
    #scheduler = ReduceLROnPlateau(optimizer, factor=0.3, patience=0, verbose=True )
    
    criterion = nn.MSELoss()
    loss_net = Loss(100, 1/100, batch_size)

    max_iter = 150

    for i in range( 0, max_iter ):
        model.train()
        e_local.clear()

        for j in range( train_data.num_batches ):
            optimizer.zero_grad()

            y_ = model( train_data.torch_in[j] )
            y = train_data.torch_out[j]

            #loss = torch.norm(y - y_, 2)
            #loss = criterion(y,y_)
            #loss = loss_func.calculate_loss(y,y_)

            c = train_data.torch_in[i][:,0]
            loss = loss_net(y,y_,c)

#            l1_regularization = torch.tensor(0)
#            for param in model.parameters():
#                 l1_regularization = l1_regularization + torch.norm(param, 1)
#            loss = loss + 0.000001*l1_regularization

            loss.backward()

            optimizer.step()
            e_local.append( loss.item() )
            #print(loss)

        train_data.shuffle_noise()
        e_av = np.sum(e_local)/len( e_local )
        print("Epoch %04d loss: %.10f" % (i + 1, e_av ) )
        torch.save( model.state_dict(), model_path )
        #scheduler.step(e_av)

    test_data = data.data(path='torch_data.npz', batch_size=1)
    model.eval()
    for i in range(0,test_data.num_batches):
        y_ = model( test_data.torch_in[i] )
        y = test_data.torch_out[i]
        #loss = criterion(y,y_)
        e = y-y_
        e_np = e.detach().numpy()
        loss = criterion(y,y_)
        print([ np.max( np.abs(e_np) ), loss.item() ])
        input()


def test_model(model_path, nn_input_size, nn_output_size):
    path = 'train_data.npz'
    test_data = data.data(0, 'blob', batch_size = 100, num_batches= 1)


#    path = 'train_data.npz'
#    test_data = data.data(0, 'blob', batch_size = 50, num_batches= 20)

    test_data.load_data(path)

    model = network( nn_input_size, nn_output_size )
    model.load_state_dict(torch.load(model_path))

    y_ = model( test_data.torch_in[0] )
    y = test_data.torch_out[0]

    e = y - y_
    e_np = e.detach().numpy()
    e_np = e_np.T
    e_np = la.norm(e_np,ord=2,axis=0)
    print(e_np)
    print(np.sum(e_np)/e_np.size )



class network_eval():
    def __init__(self, model_path):
        self.model = network( 201, 200 )
        self.model.load_state_dict(torch.load(model_path))
        self.model.eval()

    def eval_model(self, in_vec):
        out_vec = self.model( in_vec )
        return np.squeeze( np.asarray (out_vec.detach().numpy() ) )

def parse_commandline():
    parser = argparse.ArgumentParser(description='Decoder')
    parser.add_argument('-o','--operation',help='Operation', required=True)
    parser.add_argument('-mp','--model-path',help='Model Path', required=True)

    args = parser.parse_args()
    return args

if __name__ == '__main__':
    sample_size = 500
    n_field_expansion = 0

    nn_input_size = 201
    nn_output_size = 200

    train_model('model.pt', nn_input_size, nn_output_size)

    #args = parse_commandline()

    #if args.operation == 'train':
    #    train_model(args.model_path, sample_size, n_field_expansion, nn_input_size, nn_output_size)

    #if args.operation == 'test':
    #    test_model(args.model_path, nn_input_size, nn_output_size)

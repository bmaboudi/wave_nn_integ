import numpy as np
import scipy as scipy
import matplotlib.pyplot as plt
import matplotlib.animation as anim

class data_visualization():
	def __init__(self, x, data):
		self.data = data
		self.x = x

	def set_data(self, x, data):
		self.data = data
		self.x = x

	def run(self):
		fig = plt.figure()
		ax = plt.axes(xlim=(0, 1), ylim=(-0.1, 1.1))
		line, = ax.plot([], [], lw=2)

		def init():
			line.set_data([], [])
			return line,

		def animate(i):
			line.set_data(self.x, self.data[i])
			return line,

		test_anim = anim.FuncAnimation(fig, animate, init_func=init,frames=len(self.data), interval=20, blit=True)
		plt.show()


class high_fidelity():
	def __init__(self,N,c=1):
		L = 1
		self.N = N
		self.dx = L/self.N
		self.dt = self.dx/4
		self.MAX_ITER = int(N*4)

	def initialize_FD(self):
		self.initial_cond()
		self.p0 = np.zeros(self.N)
		self.stiff_mat()

	def initial_cond(self):
		x = 0

		sigma = 0.1
		self.q0 = np.zeros(self.N)
		for i in range(self.N):
			self.q0[i] = np.exp( -(x - 0.5)**2/2/sigma**2 )
			x += self.dx

	def stiff_mat(self):
		d1 = -2*np.ones(self.N)
		d2 = np.ones(self.N-1)
		self.Dxx = np.diag(d1,k=0) + np.diag(d2,k=1) + np.diag(d2,k=-1)
		self.Dxx[0,-1] = 1
		self.Dxx[-1,0] = 1
		self.Dxx /= self.dx**2

	def symplectic_euler(self,c=1):
		p = self.p0
		q = self.q0

		sol_q = [ q ]
		sol_p = [ p ]

		for i in range(self.MAX_ITER):
			q = q + self.dt * p
			p = p + self.dt * c * np.dot(self.Dxx,q)

			sol_q.append(q)
			sol_p.append(p)

#		self.compute_energy(sol_q,sol_p,c)

#		x = np.linspace(0,1,self.N)
#		anim = data_visualization(x,sol_q)
#		anim.run()
		return sol_q, sol_p

	def leap_frog(self,c=1):
		p = self.p0
		q = self.q0

		sol_q = [ q ]
		sol_p = [ p ]

		for i in range(self.MAX_ITER):
			q = q + self.dt/2 * p
			p = p + self.dt * c * np.dot(self.Dxx,q)
			q = q + self.dt/2 * p

			sol_q.append(q)
			sol_p.append(p)

#		self.compute_energy(sol_q,sol_p,c)

#		x = np.linspace(0,1,self.N)
#		anim = data_visualization(x,sol_q)
#		anim.run()
		return sol_q, sol_p

	def compute_energy(self, sol_q, sol_p, c):
		for i in range(self.MAX_ITER):
			self.Hamil(sol_q[i],sol_p[i],c)


	def Hamil(self, q, p, c):
		d1 = np.ones(self.N)
		d2 = np.ones(self.N-1)
		Dx1 = np.diag(d1,k=0) - np.diag(d2,k=-1)
		Dx1[0,-1] = -1
		Dx1 = Dx1/self.dx

		Dx2 = np.diag(d2,k=1) - np.diag(d1,k=0)
		Dx2[-1,0] = 1
		Dx2 = Dx2/self.dx

		qx1 = np.dot(Dx1,q)
		qx2 = np.dot(Dx2,q)
		pot = 0.5*c*( (qx1**2).sum() + (qx2**2).sum() )*self.dx


		kin = (p**2).sum()*self.dx
		return kin + pot

	def generated_data(self, num_data):
		in_data = []
		out_data = []
		for i in range(num_data):
			c = np.random.uniform(1,4)
			sol_q, sol_p = self.leap_frog(c)
			dim_q = sol_q[0].shape[0]

			for j in range(len(sol_q)-1):
				local_in = np.zeros( 2*dim_q + 1 )
				local_in[0] = c
				local_in[1:dim_q+1] = sol_q[j]
				local_in[dim_q+1:] = sol_p[j]

				local_out = np.zeros( 2*dim_q )
				local_out[:dim_q] = sol_q[j+1]
				local_out[dim_q:] = sol_p[j+1]

				in_data.append( local_in )
				out_data.append( local_out )

		in_data = np.reshape(in_data,[self.MAX_ITER*num_data,-1])
		out_data = np.reshape(out_data,[self.MAX_ITER*num_data,-1])

		np.savez('data_raw.npz',in_data=in_data,out_data=out_data)

if __name__ == '__main__':
	hf = high_fidelity(100)
	hf.initialize_FD()
	#hf.symplectic_euler(c=1)
	#c = np.random.uniform(1,4)
	#hf.leap_frog(c=c)

	hf.generated_data(100)
import numpy as np
import scipy as scp
import torch

class data_holder():
	def __init__(self,batch_size=100):
		self.data_in = []
		self.data_out = []
		self.torch_in = []
		self.torch_out = []

		self.batch_size = batch_size

	def read_from_file(self, path):
		raw_data = np.load(path)
		self.data_in = raw_data['in_data']
		self.data_out = raw_data['out_data']

		num_data = self.data_in.shape[0]
		self.num_batches = int( num_data/self.batch_size )
		self.num_data = self.num_batches*self.batch_size

		self.shuffle()

	def shuffle(self):
		self.torch_in.clear()
		self.torch_out.clear()

		rand_list = np.random.permutation( self.num_data )
		for i in range(self.num_batches):
			local_in = []
			local_out = []
			for j in range(self.batch_size):
				idx = rand_list[i*self.batch_size + j]
				local_in.append(self.data_in[idx])
				local_out.append(self.data_out[idx])
			local_in = np.reshape(local_in,[self.batch_size,-1])
			local_out = np.reshape(local_out,[self.batch_size,-1])
			self.torch_in.append( torch.from_numpy( local_in ).float() )
			self.torch_out.append( torch.from_numpy( local_out ).float() )

	def shuffle_noise(self):
		self.torch_in.clear()
		self.torch_out.clear()

		rand_list = np.random.permutation( self.num_data )
		for i in range(self.num_batches):
			local_in = []
			local_out = []
			for j in range(self.batch_size):
				idx = rand_list[i*self.batch_size + j]
				noise = np.random.normal(0,0.003,self.data_in[idx].shape)
				local_in.append(self.data_in[idx] + noise)
				local_out.append(self.data_out[idx])
			local_in = np.reshape(local_in,[self.batch_size,-1])
			local_out = np.reshape(local_out,[self.batch_size,-1])
			self.torch_in.append( torch.from_numpy( local_in ).float() )
			self.torch_out.append( torch.from_numpy( local_out ).float() )

	def to_torch(self):
		self.torch_in.clear()
		self.torch_out.clear()

		for i in range(self.num_batches):
			local_in = []
			local_out = []
			for j in range(self.batch_size):
				local_in.append(self.data_in[i*self.batch_size + j])
				local_out.append(self.data_out[i*self.batch_size + j])
			local_in = np.reshape(local_in,[self.batch_size,-1])
			local_out = np.reshape(local_out,[self.batch_size,-1])
			self.torch_in.append( torch.from_numpy( local_in ).float() )
			self.torch_out.append( torch.from_numpy( local_out ).float() )

	def energy(self):
		N = 100
		dx = 1/100

		d1 = torch.ones(100)
		d2 = torch.ones(100-1)

		Dx1 = torch.diag(d1,diagonal=0) - torch.diag(d2,diagonal=-1)
		Dx1[0,-1] = -1
		Dx1 = Dx1/dx
		Dx1 = torch.reshape(Dx1, [1,100,100])
		Dx1 = Dx1.repeat(self.batch_size,1,1)

		Dx2 = torch.diag(d2,diagonal=1) - torch.diag(d1,diagonal=0)
		Dx2[-1,0] = 1
		Dx2 = Dx2/dx
		Dx2 = torch.reshape(Dx2, [1,100,100])
		Dx2 = Dx2.repeat(self.batch_size,1,1)

		for i in range(self.num_batches):
			y = self.torch_out[i]
			c = self.torch_in[i][:,0]

			q = (y[:,:N]).unsqueeze(2)
			p = y[:,N:]

			qx1 = (torch.matmul(Dx1,q)).squeeze(2)
			qx2 = (torch.matmul(Dx2,q)).squeeze(2)

			pot = 0.5*c*( (qx1**2).sum(axis=1) + (qx2**2).sum(axis=1) )*dx
			kin = (p**2).sum(axis=1)*dx

if __name__ == '__main__':
	data = data_holder(batch_size=128)
	data.read_from_file('data_raw.npz')
	data.shuffle_noise()
	#data.to_torch()
	data.energy()

